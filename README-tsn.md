# Install
  - follow the README.txt for the congatec yocto BSP installation
  - supported platforms :
    - Apollo Lake : conga-QA5, conga-TCA5, conga-SA5, conga-PA5 conga-IA5, 
conga-MA5
    - Elkhart Lake : conga-QA7, conga-TCA7, conga-SA7, conga-PA7 conga-IA7,
conga-MA7
    - Tiger Lake : conga-TC570, conga-TC570r, conga-HPC/cTLH, conga-HPC/cTLU
  - the tsn currently supports i210, dp83867 and i225 ethernet interfaces (PHY)
  - the tsn also requires realtime kernel capability ( congatec-tca5-64-rt 
machine, congatec-tctl-64-rt or congatec-tca7-64-rt respectively )

# Dependencies
  URI: https://github.com/intel/iotg-yocto-bsp-public

  __layer__: iotg-yocto-bsp-public

  branch: e3900/preempt-rt

  - this guide and patches expects these revisions:
```
  039e45e21d2973fc3ce318a25beceae348e4428a  meta-intel
  da9063bdfbe130f424ba487f167da68e0ce90e7d  meta-openembedded
  dbc8727beafe689a99d8b371df98b3c842756065  meta-poky
  1ff6fdcfb971e0b5bfd037b937378fc1937b740b  meta-qt5```
# Fetch the sources
```
  cd ~/poky
  git clone https://github.com/intel/iotg-yocto-bsp-public -b e3900/preempt-rt
```

## Check iotg-yocto-bsp-public commit is MR4rt-B-01 (tag: MR4rt-B-01)
```
  cd ~/poky/iotg-yocto-bsp-public
  git log --oneline
    cbe7693 (HEAD -> e3900/preempt-rt, tag: MR4rt-B-01, origin/e3900/preempt-rt
) README: update for preempt-rt build
    d75b255 setup: enable tsn image creation
    01bf729 setup/combolayer.conf: update meta-openembedded
```
  - link layers from the iotg-yocto-bsp-public
```
    cd ~/poky
    ln -s iotg-yocto-bsp-public/meta-intel-middleware meta-intel-middleware
    ln -s iotg-yocto-bsp-public/meta-intel-tsn meta-intel-tsn
```
  - apply congatec tsn patch on the iotg-yocto-bsp-public
```
    cd ~/poky/iotg-yocto-bsp-public
    patch -p1 < ~/poky/meta-congatec-x86/patches/001-congatec-tsn.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/002-tsn-hardknott-support.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/003-tsn-hardknott-v333-support.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/004-tsn-honister-341-support.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/005-tsn-kirkstone-404-support.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/006-tsn-nanbield-support.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/007-meta-intel-menuconfig.patch
    patch -p1 < ~/poky/meta-congatec-x86/patches/008-scarthgap-501-support.patch
```
# Edit configuration
  Sample configuration files are part of the congatec bsp. Configure your 
build manually as described below or setup your project and simply copy 
*bblayers.conf.sample-tsn* and *local.conf.sample* from the bsp.

## Sample configuration
  cp ~/poky/meta-congatec-x86/bblayers.conf.sample-tsn ~/poky/build/conf/bblayers.conf

## Or edit configuration manually:
```
  nano conf/bblayers.conf
```
  - add these layers :
```
    \
    /home/<YOUR_USER_NAME>/poky/meta-intel-tsn \
    /home/<YOUR_USER_NAME>/poky/meta-intel-middleware \
    /home/<YOUR_USER_NAME>/poky/meta-openembedded/meta-multimedia \
    /home/<YOUR_USER_NAME>/poky/meta-openembedded/meta-xfce \
    /home/<YOUR_USER_NAME>/poky/meta-openembedded/meta-filesystems \
  "
```
## Image configuration
```
  cp ~/poky/meta-congatec-x86/local.conf.sample ~/poky/build/conf/local.conf
  nano conf/local.conf
```
  - select platform support with realtime kernel :
    - Apollo Lake :
```
        MACHINE ?= "congatec-tca5-64-rt"
```
    - Elkhart Lake :
```
        MACHINE ?= "congatec-tca7-64-rt"
```
    - Tiger Lake :
```
        MACHINE ?= "congatec-tctl-64-rt"
```
  - finally include a test tools
```
    IMAGE_INSTALL:append = " tsn-talker udp-tai "
```
  - wayland requires pam
```
    DISTRO_FEATURES:append = " pam"
```
# Build
```
  bitbake core-image-rt-tsn-sdk
```
# Test
  The BSP includes TSN use-case examples - document *doc/tsn-test-guide.md*.

  The document is based on based on intel TSN guide [1].

  Four tests are described :
    1. IEEE 802.1AS-2011 - generalized Precision Time Protocol (gPTP)
    2. IEEE 802.1Qav - Forwarding and Queuing Enhancements for Time-Sensitive Streams (FQTSS) - Credit-Based Shaper (CBS)
    ~~3. IEEE 802.1Qbv - Enhancements to Traffic Scheduling: Time-Aware Shaper (TAS)~~
    4. Time-Based Scheduling (TBS) - Earliest Tx-Time First (ETF)

# Known Issues
  1. The test number 3 "IEEE 802.1Qbv - Enhancements to Traffic Scheduling: Time-Aware Shaper (TAS)" fails for kernel 6.6
    Solution: Currently unknown, functionality is temporarily unavailable for scarthgap release.

# Resources
  [1] - Time-Sensitive Networking (TSN) Reference Software for Linux, intel March 2020, Doc.No.: 605583, Rev.: 1.2
