PREFERRED_PROVIDER_virtual/kernel ?= "linux-intel"
PREFERRED_VERSION_linux-intel ?= "6.8%"
PREFERRED_VERSION_linux-intel-rt ?= "6.6%"

require conf/machine/include/meta-intel.inc
require conf/machine/include/intel-corei7-64-common.inc
require conf/machine/include/intel-common-pkgarch.inc

MACHINE_FEATURES += "pcbios efi"
MACHINE_FEATURES += "wifi 3g nfc"
MACHINE_FEATURES += "intel-ucode"

MACHINE_HWCODECS ?= "${@bb.utils.contains('TUNE_FEATURES', 'mx32', '', 'intel-media-driver intel-mediasdk', d)} gstreamer1.0-vaapi"

MACHINE_EXTRA_RRECOMMENDS += "linux-firmware cgos"

# Enable optional dpdk:
COMPATIBLE_MACHINE_pn-dpdk = "intel-corei7-64"
COMPATIBLE_MACHINE:pn-dpdk-module = "intel-corei7-64"

XSERVER ?= "${XSERVER_X86_BASE} \
            ${XSERVER_X86_EXT} \
            ${XSERVER_X86_FBDEV} \
            ${XSERVER_X86_I915} \
            ${XSERVER_X86_I965} \
            ${XSERVER_X86_MODESETTING} \
            ${XSERVER_X86_VESA} \
            ${XSERVER_X86_ASPEED_AST} \
           "

SYSLINUX_OPTS = "serial 0 115200"
APPEND += "rootwait"

IMAGE_FSTYPES += " wic wic.bmap"
IMAGE_INSTALL:append = " cgos bmaptool kernel-module-cgosdrv"
WKS_FILE ?= "${@bb.utils.contains_any("EFI_PROVIDER", "systemd-boot", "systemd-bootdisk-microcode.wks.in", "grub-bootdisk-microcode.wks.in", d)}"
WKS_FILE_DEPENDS:append = " intel-microcode"
