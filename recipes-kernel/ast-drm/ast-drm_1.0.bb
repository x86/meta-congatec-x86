SUMMARY = "ASPEED Graphics Linux DRM Driver"
DESCRIPTION = "${SUMMARY}"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${WORKDIR}/Linux_DRM_1.14.0_4/COPYING;md5=f95ba9745406e4c4c46843b63ecaa370"

inherit module

#SRC_URI = "https://www.aspeedtech.com/file/support/Linux_DRM_1.14.0_4.tar.gz "
SRC_URI = "file://Linux_DRM_1.14.0_4.tar.gz \
           file://001-k515.patch \
"

S = "${WORKDIR}/Linux_DRM_1.14.0_4/sources/src515"
EXTRA_OEMAKE = " KERNELDIR=${STAGING_KERNEL_DIR} KERNELINST=${D} "

RPROVIDES_${PN} += "kernel-module-ast-drm"

