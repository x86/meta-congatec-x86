FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# EHL
SRC_URI:append:congatec-tca7-64-rt = " file://ehl-plat-001.cfg"

# EHL ethernet
SRC_URI:append:congatec-tca7-64-rt = " file://net-phy-dp83867.cfg"
# 0001-net-phy-dp83867.patch is required for y404 / k5.15 - system suspend - k6.4 merge
#SRC_URI:append:congatec-tca7-64-rt = " file://0001-net-phy-dp83867-k6.4.patch"
# 0002-pinctrl-intel.patch is required for BIOS xA70R0014 and older - fix pinctrl kernel panic when system boot
#SRC_URI:append:congatec-tca7-64-rt = " file://0002-pinctrl-intel.patch"

#cgos vmalloc
SRC_URI:append = " file://cgos-vmalloc-001.patch"
