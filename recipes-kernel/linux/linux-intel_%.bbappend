FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
SRC_URI:append = " file://eth_igb.cfg"
SRC_URI:append = " file://hd-audio.cfg"

# DSAC
SRC_URI:append:congatec-b7ac-64 = " file://x2apic.cfg"
SRC_URI:append:congatec-b7ac-64 = " file://ixgbe.cfg"
#SRC_URI:append:append = " file://cg-uart.patch"

# APL - SPI
SRC_URI:append:append:congatec-tca5-64 = " file://apl-spi-001.cfg"

# JCWL ethernet
SRC_URI:append:append:congatec-tcwl-64 = " file://igc.cfg"

# EHL
SRC_URI:append:congatec-tca7-64 = " file://ehl-plat-001.cfg"

# EHL ethernet
SRC_URI:append:congatec-tca7-64 = " file://net-phy-dp83867.cfg"
# 0001-net-phy-dp83867.patch is required for y404 / k5.15 - system suspend - k6.4 merge
#SRC_URI:append:congatec-tca7-64 = " file://0001-net-phy-dp83867.patch"
#NOK SRC_URI:append:congatec-tca7-64 = " file://0001-net-phy-dp83867-k6.4.patch"

# 0002-pinctrl-intel.patch is required for BIOS xA70R0014 and later - fix pinctrl kernel panic when system boot
#SRC_URI:append:congatec-tca7-64 = " file://0003-pinctrl-intel-k6-8.patch"

#cgos vmalloc
SRC_URI:append = " file://cgos-vmalloc-001.patch"

# DICL
SRC_URI:append:congatec-dicl-64 = " file://0004-intel-ice.cfg"

# HSIL
SRC_URI:append:congatec-hsil-64 = " file://0003-ast-drm_menuconfig.patch"
SRC_URI:append:congatec-hsil-64 = " file://hisl-ast-drm.cfg"
SRC_URI:append:congatec-hsil-64 = " file://0004-intel-ice.cfg"

# HEIH
SRC_URI:append:congatec-heih-64 = " file://0003-ast-drm_menuconfig.patch"
SRC_URI:append:congatec-heih-64 = " file://hisl-ast-drm.cfg"
SRC_URI:append:congatec-heih-64 = " file://0004-intel-ice.cfg"
