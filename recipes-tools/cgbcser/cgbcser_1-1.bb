SUMMARY = "Example of how to build an external Linux kernel module"
DESCRIPTION = "${SUMMARY}"
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD-2-Clause;md5=cb641bc04cda31daea161b1bc15da69f"

inherit module

SRC_URI = "file://Makefile \
           file://cgbcser.c \
           file://001-build.patch \
          "

S = "${WORKDIR}"
RPROVIDES_${PN} += "kernel-module-${PN}"
