DESCRIPTION = "Congatec System Utility"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/CGUTIL_170/cgutillx/COPYING_BSD2;md5=c6a44ab2e31654333037bac447565701"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r0"
S = "${WORKDIR}/CGUTIL_170/cgutillx"
DEPENDS = "cgos"
RDEPENDS:${PN} = " cgos"

SRC_URI = "file://CGUTIL_170.tar.bz2 \
	   file://001-cgutillx-170-build.patch \
 "

FILES:${PN} += "${bindir}/CGUTIL_170/cgutlcmd"


do_compile() {
  env
  cd cgutlcmd
  oe_runmake
}

do_install() {
  install -d ${D}/${bindir}
  install -m 0755 ${WORKDIR}/CGUTIL_170/cgutillx/cgutlcmd/cgutlcmd ${D}/${bindir}
}

PARALLEL_MAKE = ""

INSANE_SKIP:${PN} = "ldflags"
