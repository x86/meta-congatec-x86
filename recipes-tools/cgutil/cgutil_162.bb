DESCRIPTION = "Congatec System Utility"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgutillx/COPYING_BSD2;md5=9b3125bcd0c34c604cc5cad03b8cfd27"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r3"

DEPENDS = "cgos"
RDEPENDS:${PN} = " cgos"

SRC_URI = "file://cgutillx_158_2.tar \
           file://cgutillx-158-to-161.patch \
	   file://001-cgutillx-162-build.patch \
	   file://cgutillx-162.patch \
          "

FILES:${PN} += "${bindir}/cgutlcmd"

S = "${WORKDIR}/cgutillx"

do_compile() {
  env
  cd cgutlcmd
  oe_runmake
}

do_install() {
  install -d ${D}/${bindir}
  install -m 0755 ${WORKDIR}/cgutillx/cgutlcmd/cgutlcmd ${D}/${bindir}
}

PARALLEL_MAKE = ""

INSANE_SKIP:${PN} = "ldflags"
