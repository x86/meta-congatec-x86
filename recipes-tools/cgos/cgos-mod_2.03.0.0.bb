DESCRIPTION = "Congatec OS API (CGOS) support"
AUTHOR = "jan.janson@congatec.com"
SECTION = "libs"

HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${WORKDIR}/CGOS_DIRECT_Lx_R3.0.0/CgosDrv/COPYING_GPL2;md5=b234ee4d69f5fce4486a80fdaf4a4263"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r3"

inherit module kernel-module-split

SRC_URI = "file://CGOS_DIRECT_Lx_R3.0.0.42a7ead.tar.bz2 \
           file://001-yocto-build.patch \
  "

S = "${WORKDIR}/CGOS_DIRECT_Lx_R3.0.0"
EXTRA_OEMAKE = " KERNELDIR=${STAGING_KERNEL_DIR} KERNELINST=${D} "
MODULES_MODULE_SYMVERS_LOCATION = "CgosDrv/Lx"

MODULE_NAME = "cgosdrv"
MAKE_TARGETS = "mod"
MODULES_INSTALL_TARGET = "install_mod"
PKG_${PN} = "kernel-module-${MODULE_NAME}"
PARALLEL_MAKE = ""
MACHINE_EXTRA_RRECOMMENDS += "kernel-module-${MODULE_NAME}"

module_do_install:append() {
    install -d ${D}${nonarch_base_libdir}/udev/rules.d
    install -d ${D}${libdir}/modules-load.d
    cp ${S}/99-cgos.rules ${D}${nonarch_base_libdir}/udev/rules.d/99-cgos.rules
    cp ${S}/cgos.conf ${D}${libdir}/modules-load.d/cgos.conf
    install -d ${D}${libdir}/modules/${KERNEL_VERSION}/extra
    cp ${S}/CgosDrv/Lx/cgosdrv.ko ${D}${libdir}/modules/${KERNEL_VERSION}/extra/cgosdrv.ko
}

FILES:${PN} += "${libdir}/modules-load.d/cgos.conf \
                ${nonarch_base_libdir}/udev/rules.d/99-cgos.rules \
		${libdir}/modules/${KERNEL_VERSION}/extra/cgosdrv.ko \
"

RPROVIDES:${PN} += "kernel-module-cgosdrv"
