DESCRIPTION = "Congatec OS API (CGOS) support"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgoslx-x64-1.03.025/CgosLib/COPYING_BSD2;md5=2d27e8504c0c076f604994ed9593e4c6"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r2"

DEPENDS = " cgos-mod"
RDEPENDS:${PN} = " cgos-mod"

SRC_URI = "file://cgoslx-x64-1.03.025.tar.gz \
           file://cgoslx-x64-1.03.025.patch \
           file://cgoslx-x64-1.03.031.patch \
           file://cgoslx-x64-1.03.031_y341.patch \
           file://cgoslx-x64-1.03.032.patch \
          "

PACKAGES = "${PN} ${PN}-dev ${PN}-dbg"
PROVIDES = "${PACKAGES}"

FILES:${PN} += "${bindir}/cgosdump \
                ${bindir}/cgostest \
                ${bindir}/cgosmon \
                ${libdir}/libcgos.so \
               "

FILES:${PN}-dev += " ${includedir}/Cgos.h \
                     ${libdir}/libcgos.so \
                   "

FILES:${PN}-dbg += " ${libdir}/.debug/* \
                     ${bindir}/.debug/* \
                   "

S = "${WORKDIR}/cgoslx-x64-1.03.025"
EXTRA_OEMAKE = " INST_BIN=${D}${bindir} INST_LIB=${D}${libdir}"

do_compile() {
  oe_runmake app
}

do_install() {
  oe_runmake install_app

  install -d ${D}/${includedir}
  install -m 0644 ${S}/CgosLib/Cgos.h ${D}/${includedir}
  install -m 0755 ${S}/CgosLib/Lx/libcgos.so ${D}/${libdir}
}

PARALLEL_MAKE = ""

INSANE_SKIP:${PN} = "ldflags"
#PREFERRED_VERSION_cgos-mod = ${PV}
