DESCRIPTION = "Congatec OS API (CGOS) support"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"

# Note: this recipe links the executables against loader in /lib64 which possibly can be missing.
# Solution is link /lib64 to /lib in the rootfs, this needs image recipe modification (see IMAGE_PREPROCESS_COMMAND)

HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/CGOS_DIRECT_Lx_R3.0.0/CgosLib/COPYING_BSD2;md5=ed45fbc9a3094e85642610390226ff9c"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r0"

DEPENDS = " cgos-mod"
RDEPENDS_${PN} = " cgos-mod"
PREFERRED_VERSION_cgos-mod = "2.3.0.0"

SRC_URI = "file://CGOS_DIRECT_Lx_R3.0.0.42a7ead.tar.bz2 \
           file://001-yocto-build.patch \
  "

PACKAGES = "${PN} ${PN}-dev ${PN}-dbg"
PROVIDES = "${PACKAGES}"

FILES:${PN} += "${bindir}/cgosdump \
                ${bindir}/cgosmon \
                ${bindir}/cgostest \
                ${libdir}/libcgos.so \
               "

FILES:${PN}-dev += " ${includedir}/Cgos.h \
                     ${libdir}/libcgos.so \
                   "

FILES:${PN}-dbg += " ${libdir}/.debug/* \
                     ${bindir}/.debug/* \
                   "

S = "${WORKDIR}/CGOS_DIRECT_Lx_R3.0.0"
EXTRA_OEMAKE = " INST_BIN=${D}${bindir} INST_LIB=${D}${libdir}"

do_compile() {
  oe_runmake app
}

do_install() {
  oe_runmake install_app

  install -d ${D}/${includedir}

  install -m 0644 ${S}/CgosLib/Cgos.h ${D}/${includedir}
  install -m 0755 ${S}/CgosLib/Lx/libcgos.so ${D}/${libdir}
}

PARALLEL_MAKE = ""

INSANE_SKIP:${PN} = "ldflags"
