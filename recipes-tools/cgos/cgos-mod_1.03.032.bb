DESCRIPTION = "Congatec OS API (CGOS) support"
SECTION = "libs"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgoslx-x64-1.03.025/CgosDrv/COPYING_GPL2;md5=b234ee4d69f5fce4486a80fdaf4a4263"

COMPATIBLE_MACHINE = "congatec-(tca5|tca7|b7ac|tsco|tcwl|tctl|dicl|hsil|galp|gals|heih)-64"

PR = "r2"

inherit module kernel-module-split

SRC_URI = "file://cgoslx-x64-1.03.025.tar.gz \
           file://cgoslx-x64-1.03.025.patch \
           file://cgoslx-x64-1.03.031.patch \
           file://cgoslx-x64-1.03.031_y341.patch \
           file://cgoslx-x64-1.03.032.patch \
           file://cgos-mod_001.patch \
           file://cgos-mod-vmalloc.patch \
          "

S = "${WORKDIR}/cgoslx-x64-1.03.025"
EXTRA_OEMAKE = " KERNELDIR=${STAGING_KERNEL_DIR} KERNELINST=${D} "
MODULES_MODULE_SYMVERS_LOCATION = "CgosDrv/Lx"

MODULE_NAME = "cgosdrv"
MAKE_TARGETS = "mod"
MODULES_INSTALL_TARGET = "install_mod"
PKG_${PN} = "kernel-module-${MODULE_NAME}"
PARALLEL_MAKE = ""
MACHINE_EXTRA_RRECOMMENDS += "kernel-module-${MODULE_NAME}"

module_do_install:append() {
    install -d ${D}${nonarch_base_libdir}/udev/rules.d
    install -d ${D}${libdir}/modules-load.d
    cp ${S}/99-cgos.rules ${D}${nonarch_base_libdir}/udev/rules.d/99-cgos.rules
    cp ${S}/cgos.conf ${D}${libdir}/modules-load.d/cgos.conf
}

FILES:${PN} += "${libdir}/modules-load.d/cgos.conf \
                ${nonarch_base_libdir}/udev/rules.d/99-cgos.rules \
"

RPROVIDES:${PN} += "kernel-module-cgosdrv"
