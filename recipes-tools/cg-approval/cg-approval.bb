DESCRIPTION = "Congatec Approval Test Dependencies"
AUTHOR = "jan.janson@congatec.com"
SECTION = "devel"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${WORKDIR}/COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

PR = "r6"
ALLOW_EMPTY:${PN} = "1"

SRC_URI = "file://COPYING \
           file://cg-approval.txt \
"

RRECOMMENDS:${PN} = " systemd cgutil alsa-utils setserial pciutils \
  systemd-compat-units ethtool dropbear util-linux kernel-modules \
  tcpdump iperf3 \
  networkmanager screen usb-modeswitch usbutils \
  nano bash expect socat \
  conntrack-tools iputils iproute2 \
  cgutil "

S = "${WORKDIR}"

do_install() {
  install -d ${D}/${datadir}
  install -m 0444 ${WORKDIR}/cg-approval.txt ${D}/${datadir}
}

FILES:${PN} += "${datadir}/cg-approval.txt"

