# TSN Test Topology
**DUT**------**ethernet**-----**Counterpart**

**DUT** = Device under Test
**Counterpart** = TSN verified Counterpart
**Host Machine** (+Wireshark)

The TSN Test is based and verified on Intel i210 and i225 controllers. Other TSN capable controller may differ in the Qdisc configurations of TAPRIO and MQPRIO.

# 1. IEEE 802.1AS-2011 - generalized Precision Time Protocol (gPTP)
## 1.1. Restart DUT and Counterpart
## 1.2. Check the Timestamping Capabilities of the NIC with ethtool
**[DUT]**
  `$ ethtool -T enp1s0`

  Time stamping parameters for enp1s0:
```
  Capabilities:
    hardware-transmit     (SOF_TIMESTAMPING_TX_HARDWARE)
    software-transmit     (SOF_TIMESTAMPING_TX_SOFTWARE)
    hardware-receive      (SOF_TIMESTAMPING_RX_HARDWARE)
    software-receive      (SOF_TIMESTAMPING_RX_SOFTWARE)
    software-system-clock (SOF_TIMESTAMPING_SOFTWARE)
    hardware-raw-clock    (SOF_TIMESTAMPING_RAW_HARDWARE)
  PTP Hardware Clock: 1
  Hardware Transmit Timestamp Modes:
    off                   (HWTSTAMP_TX_OFF)
    on                    (HWTSTAMP_TX_ON)
  Hardware Receive Filter Modes:
    none                  (HWTSTAMP_FILTER_NONE)
    all                   (HWTSTAMP_FILTER_ALL)
```

## 1.3. Disable TSO
**[DUT + Counterpart]**

  `$ ethtool -K enp1s0 tso off`

## 1.4. Disable EEE
**[DUT + Counterpart]**
```
  $ ethtool --set-eee enp1s0 eee off
  $ ethtool --show-eee enp1s0
  
  EEE Settings for enp1s0:
  EEE status: disabled
  Tx LPI: disabled
  Supported EEE link modes: 100baseT/Full
                            1000baseT/Full
  Advertised EEE link modes: Not reported
  Link partner advertised EEE link modes: Not reported
```

## 1.5. Start ptp4l with the gPTP.cfg profile, which represents the 801.1AS Standard
The file gPTP.cfg (available in configs folder of Linux PTP source).
  `~/build/tmp/work/corei7-64-poky-linux/linuxptp/2.0-r0/linuxptp-2.0/configs/gPTP.cfg`
```
--- gPTP.cfg ---
#
# 802.1AS example configuration containing those attributes which
# differ from the defaults.  See the file, default.cfg, for the
# complete list of available options.
#
[global]
gmCapable               1
priority1               248
priority2               248
logAnnounceInterval     0
logSyncInterval         -3
syncReceiptTimeout      3
neighborPropDelayThresh 800
min_neighbor_prop_delay -20000000
assume_two_step         1
path_trace_enabled      1
follow_up_info          1
transportSpecific       0x1
ptp_dst_mac             01:80:C2:00:00:0E
network_transport       L2
delay_mechanism         P2P
--- end ---
```

**[DUT]**
Start the DUT as Master.
```
  $ ptp4l -P2Hi enp1s0 -f gPTP.cfg --step_threshold=1 -m
  
  DUT output:
  ptp4l[2612.684]: selected /dev/ptp1 as PTP clock
  ptp4l[2612.701]: port 1: INITIALIZING to LISTENING on INIT_COMPLETE
  ptp4l[2612.702]: port 0: INITIALIZING to LISTENING on INIT_COMPLETE
  ptp4l[2615.969]: port 1: new foreign master 6cb311.fffe.521c17-1
  ptp4l[2616.258]: port 1: LISTENING to MASTER on ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES
  ptp4l[2616.258]: selected local clock 6cb311.fffe.521c10 as best master
  ptp4l[2616.258]: assuming the grand master role
  ptp4l[2617.969]: selected best master clock 6cb311.fffe.521c17
  ptp4l[2617.969]: assuming the grand master role
```

**[Counterpart]**
Start the Counterpart as Slave.
```
  $ ptp4l -P2Hi enp1s0 -f gPTP.cfg --step_threshold=1 -m -s

  ptp4l[2612.064]: selected /dev/ptp1 as PTP clock
  ptp4l[2612.085]: port 1: INITIALIZING to LISTENING on INIT_COMPLETE
  ptp4l[2612.085]: port 0: INITIALIZING to LISTENING on INIT_COMPLETE
  ptp4l[2615.483]: port 1: LISTENING to MASTER on ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES
  ptp4l[2615.483]: selected local clock 6cb311.fffe.521c17 as best master
  ptp4l[2615.483]: assuming the grand master role
  ptp4l[2617.775]: port 1: new foreign master 6cb311.fffe.521c10-1
  ptp4l[2619.776]: selected best master clock 6cb311.fffe.521c10
  ptp4l[2619.776]: port 1: MASTER to UNCALIBRATED on RS_SLAVE
  ptp4l[2620.277]: port 1: UNCALIBRATED to SLAVE on MASTER_CLOCK_SELECTED
  ptp4l[2621.028]: rms 82 max 106 freq -13587 +/- 59 delay 164 +/- 0
  ptp4l[2622.029]: rms 13 max 23 freq -13541 +/- 18 delay 164 +/- 0
  ptp4l[2623.030]: rms 20 max 24 freq -13504 +/- 6 delay 164 +/- 0
  ptp4l[2624.031]: rms 14 max 17 freq -13495 +/- 4 delay 164 +/- 0
  ptp4l[2625.032]: rms 6 max 8 freq -13495 +/- 3 delay 164 +/- 0
  ptp4l[2626.033]: rms 4 max 9 freq -13505 +/- 5 delay 164 +/- 0
  ptp4l[2627.034]: rms 3 max 5 freq -13506 +/- 4 delay 164 +/- 0
  ptp4l[2628.035]: rms 3 max 6 freq -13507 +/- 4 delay 164 +/- 0
  ptp4l[2629.036]: rms 3 max 6 freq -13509 +/- 4 delay 164 +/- 0
  ptp4l[2630.037]: rms 3 max 5 freq -13503 +/- 4 delay 164 +/- 0
  ptp4l[2631.038]: rms 3 max 4 freq -13505 +/- 4 delay 164 +/- 0
  ptp4l[2632.039]: rms 4 max 6 freq -13510 +/- 4 delay 164 +/- 0
```  
The rms value reported by ptp4l (once the slave has locked with the GM) shows the root mean square of the time offset between the PHC and the GM clock. The PHC is synchronized if the ptp4l consistently reports rms lower than 100 ns.

## 1.6. Set system clocks properly
**[DUT + Counterpart]**
```
  $ pmc -u -b 0 -t 1 "SET GRANDMASTER_SETTINGS_NP clockClass 248 clockAccuracy \
  0xfe offsetScaledLogVariance  0xffff currentUtcOffset 37 leap61 0 leap59 0 \
  currentUtcOffsetValid 1 ptpTimescale 1 timeTraceable 1 frequencyTraceable 0 \
  timeSource 0xa0"
```

## 1.7. Disable NTP
**[DUT + Counterpart]**  
  `$ timedatectl set-ntp 0`

Synchronize Systemclock with the PHY clock.

**[DUT + Counterpart]**
```
  $ phc2sys -s enp1s0 -c CLOCK_REALTIME --step_threshold=1 --transportSpecific=1 -w -m

  phc2sys[2675.661]: CLOCK_REALTIME phc offset -475 s0 freq -26196 delay 4750
  phc2sys[2676.661]: CLOCK_REALTIME phc offset -495 s2 freq -26216 delay 4758
  phc2sys[2677.662]: CLOCK_REALTIME phc offset -489 s2 freq -26705 delay 4758
  phc2sys[2678.662]: CLOCK_REALTIME phc offset 11 s2 freq -26352 delay 4759
  phc2sys[2679.662]: CLOCK_REALTIME phc offset 192 s2 freq -26167 delay 4754
  phc2sys[2680.662]: CLOCK_REALTIME phc offset 131 s2 freq -26171 delay 4739
  phc2sys[2681.663]: CLOCK_REALTIME phc offset 93 s2 freq -26169 delay 4750
  phc2sys[2682.663]: CLOCK_REALTIME phc offset -2 s2 freq -26237 delay 4667
  phc2sys[2683.663]: CLOCK_REALTIME phc offset 72 s2 freq -26163 delay 4766
  phc2sys[2684.663]: CLOCK_REALTIME phc offset 40 s2 freq -26174 delay 4760
  phc2sys[2685.664]: CLOCK_REALTIME phc offset 6 s2 freq -26196 delay 4754
```
The offset information reported by phc2sys shows the time offset between the PHC and the System clock.  
The phc offset is contantly decreasing until it achieves a reasonable range of offset.

# 2. IEEE 802.1Qav - Forwarding and Queuing Enhancements for Time-Sensitive Streams (FQTSS) - Credit-Based Shaper (CBS)
## 2.1. Restart DUT and Counterpart
## 2.2. Assign static IP Adresses
**[DUT & Counterpart]**
```
  systemctl stop systemd-networkd.socket
  systemctl stop systemd-networkd.service
```

**[DUT]**  
  `$ ifconfig enp1s0 inet 169.254.0.1 netmask 255.255.255.0`
  
**[Counterpart]**  
  `$ ifconfig enp1s0 inet 169.254.0.2 netmask 255.255.255.0`
  
## 2.3. Check available queues
**[DUT]**
```
  $ ls /sys/class/net/enp1s0/queues/

  rx-0 rx-1 rx-2 rx-3 tx-0 tx-1 tx-2 tx-3
```

## 2.4. Setup Traffic Class with MQPRIO
**[DUT]**
```
  $ tc qdisc replace dev enp1s0 handle 100: parent root \
  mqprio num_tc 4 map 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 3 \
  queues 1@0 1@1 1@2 1@3 hw 0
```

## 2.5. Set Queue 2 Priority 2 10% Bandwidth of 1Gbps with the CBS Qdisc
**[DUT]**
```
  $ tc qdisc replace dev enp1s0 parent 100:3 cbs \
  locredit -1350 \
  hicredit 150 \
  sendslope -900000 \
  idleslope 100000
```  
Show qdisc configuration.

**[DUT]**
```
  $ tc -g class show dev enp1s0
  +---(100:ffe3) mqprio
  | +---(100:4) mqprio
  |
  +---(100:ffe2) mqprio
  | +---(100:3) mqprio
  |
  +---(100:ffe1) mqprio
  | +---(100:2) mqprio
  |
  +---(100:ffe0) mqprio
    +---(100:1) mqprio
```
## 2.7. Send UDP packets of 1500 bytes using socket priority 2
**[DUT]**  
  `$ tsn-talker -d 01:AA:AA:AA:AA:AA -i enp1s0 -p 2 -s 1500`

## 2.8. Check Queue 2 statistics and verify if the <Sent packet> is increasing.
**[DUT]**  
  `$ tc -s qdisc show dev enp1s0 | grep 100:3 -A3`

## 2.9. Start tsn-listsner
**[Counterpart]**  
  `$ tsn-listener -d 01:AA:AA:AA:AA:AA -i enp1s0 -s 1500`

## 2.10. Capture Packets using tcpdump on Counterpart
**[Counterpart]**  
  `$ tcpdump -Q in -nvvvXXi enp1s0 --time-stamp-precision=nano -j adapter_unsynced -B 524288 -s 54 -w qav.pcap`
   - Start the iperf3 server

**[Counterpart]**  
  `$ iperf3 -s -B 169.254.0.2`

## 2.11. Start the iperf3 client
**[DUT]**  
  `$ iperf3 -c 169.254.0.2 -t 60`

## 2.12. Check Bandwidth on Counterpart side
Check on Counterpart in the tsn-listener terminal, if the Bandwidth stays stable around the reserved bandwidth 1.5 Mbit/s (for Ubuntu 100 Mbit/s), while iperf3 creates network congestion.
```
  Receiving data rate: 975324 kbps 
  Receiving data rate: 975180 kbps
  Receiving data rate: 975348 kbps
  Receiving data rate: 975384 kbps
  Receiving data rate: 975252 kbps
  Receiving data rate: 975300 kbps
  Receiving data rate: 975216 kbps
  Receiving data rate: 975288 kbps
```

## 2.13. (Optional) Analyze the qav.pcap on a host Machine with Wireshark
**[Host]**  
  - Open Wireshark
  - Statistics ---> I/O Graph
  - Add Display Filters
      `tcp.dstport == 5201`
  
      `eth.addr == 01:aa:aa:aa:aa:aa`
  - Set 'Y Axis' (see the table) to Bitrate
  
Next image should visualize that the iperf3 TCP Traffic is not interfering with the reserved 100 Mbit/s bandwidth of the Time Sensitive AVTP Traffic.  
![alt text](doc/images/test-no2-results.png "Reserved bandwidth")
![alt text](doc/images/test-no2-results-b.png "AV stream detail")

# 3. IEEE 802.1Qbv - Enhancements to Traffic Scheduling: Time-Aware Shaper (TAS)
## 3.1. Restart DUT and Counterpart
## 3.2. Assign static IP adresses

**[DUT & Counterpart]**
```
  systemctl stop systemd-networkd.socket
  systemctl stop systemd-networkd.service
```

**[DUT]**  
  `$ ifconfig enp1s0 inet 169.254.0.1 netmask 255.255.255.0`
  
**[Counterpart]**  
  `$ ifconfig enp1s0 inet 169.254.0.2 netmask 255.255.255.0`
  
## 3.3. Start iperf3 server on Counterpart
**[Counterpart]**  
  `$ iperf3 -s -p 5100 & iperf3 -s -p 5200 & iperf3 -s -p 5300 & iperf3 -s -p 5400`

## 3.4. capture packets using tcpdump
**[Counterpart]**  
  `$ tcpdump -Q in -nvvvXXi enp1s0 --time-stamp-precision=nano -j adapter_unsynced -B 524288 -s 54 -w qbv.pcap`

## 3.5. Start the PTP hardware clock (PHC) on DUT
**[DUT]**  
  `$ ptp4l -i enp1s0 -m -2`

## 3.6. Synchronize the System Clock to the PTP hardware clock (PHC).
**[DUT]**  
  `$ phc2sys -s enp1s0 -m -w -O -0 CLOCK_REALTIME`
  
## 3.7. Setup TAPRIO qdisc, Start the schedule 5 seconds into 5 the future
Queue setup :
- queue0 is open for 5 ms
- queue1 is open for 5 ms
- queue2 is open for 5 ms
- queue3 is open for 5 ms
    
**[DUT]**
```
  $ BASE=$(expr $(date +%s) + 5)000000000
  echo "$BASE"
  tc qdisc add dev enp1s0 parent root handle 100 taprio \
  num_tc 4 \
  map 3 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 \
  queues 1@0 1@1 1@2 1@3 \
  base-time $BASE \
  sched-entry S 01 5000000 \
  sched-entry S 02 5000000 \
  sched-entry S 04 5000000 \
  sched-entry S 08 5000000 \
  clockid CLOCK_TAI
```

## 3.8. Modifiy Socket Priority
Since we cannot modify iperf3’s socket priority, use net_prio cgroups to control which hardware transmit queue each iperf3 instance is using. 
The default without cgroup is 0, so create 4 more cgroups for q0, q1, q2 and q3.  
**[DUT]**
```
  $ mkdir -p /sys/fs/cgroup/net_prio/grp1
  $ mkdir -p /sys/fs/cgroup/net_prio/grp2
  $ mkdir -p /sys/fs/cgroup/net_prio/grp3
  $ mkdir -p /sys/fs/cgroup/net_prio/grp4
  $ echo "enp1s0 1" > /sys/fs/cgroup/net_prio/grp1/net_prio.ifpriomap
  $ echo "enp1s0 2" > /sys/fs/cgroup/net_prio/grp2/net_prio.ifpriomap
  $ echo "enp1s0 3" > /sys/fs/cgroup/net_prio/grp3/net_prio.ifpriomap
  $ echo "enp1s0 4" > /sys/fs/cgroup/net_prio/grp4/net_prio.ifpriomap
```

## 3.9. Run iperf3 client and add each instance to its own cgroup directory.
**[DUT]**
```
  $ export TARGET_IP=169.254.0.2
  $ iperf3 -u -l1000 -b0 -p 5100 -c $TARGET_IP -t 60 >> /dev/null &
  $ echo $! > /sys/fs/cgroup/net_prio/grp1/cgroup.procs
  $ iperf3 -u -l1000 -b0 -p 5200 -c $TARGET_IP -t 60 >> /dev/null &
  $ echo $! > /sys/fs/cgroup/net_prio/grp2/cgroup.procs
  $ iperf3 -u -l1000 -b0 -p 5300 -c $TARGET_IP -t 60 >> /dev/null &
  $ echo $! > /sys/fs/cgroup/net_prio/grp3/cgroup.procs
  $ iperf3 -u -l1000 -b0 -p 5400 -c $TARGET_IP -t 60 >> /dev/null &
  $ echo $! > /sys/fs/cgroup/net_prio/grp4/cgroup.procs
```

## 3.10. Watch the queue statistic every 0.5 seconds.
**[DUT]**  
  `$ watch -d -n 0.5 "tc -s qdisc show dev enp1s0"`

## 3.11. Verifying Qbv in Wireshark
**[Host]**
  - Open Wireshark
  - Statistics ---> I/O Graph
  - Add Display Filters
```
  udp.dstport == 5100
  udp.dstport == 5200
  udp.dstport == 5300
  udp.dstport == 5400
```
  - Set Interval to 1ms
  - Zoom into the x axis into the millisecond range
![alt text](doc/images/test-no3-results.png "Time slots 5ms")
  
In the image above you can see iperf udp packets are transmitted strictly according to the TAPRIO gates schedule. Each queue is open for 5ms. This gives us a cycle time 20 ms for 4 queues.  
Exact timing is visible in the packet list. Please see the time between first queue1 packet and first queue2 packet - this should be 5ms. The cycle time can be measured between first queue1 packet and last queue4 packet - this should match 20 ms.
  
# 4. Time-Based Scheduling (TBS) - Earliest Tx-Time First (ETF)
## 4.1. Restart DUT and Counterpart
## 4.2. Disable TSO
**[DUT + Counterpart]**  
  `$ ethtool -K enp1s0 tso off`
  
## 4.3. Disable EEE
**[DUT + Counterpart]**
```
  $ ethtool --set-eee enp1s0 eee off
  $ ethtool --show-eee enp1s0

    EEE Settings for enp1s0:
        EEE status: disabled
        Tx LPI: disabled
        Supported EEE link modes:  100baseT/Full
                                   1000baseT/Full
        Advertised EEE link modes:  Not reported
        Link partner advertised EEE link modes:  Not reported
```

## 4.4. Start ptp4l with the gPTP.cfg profile, which represents the 801.1AS Standard
**[DUT + Counterpart]**  
  `$ ptp4l -P2Hi enp1s0 -f gPTP.cfg --step_threshold=1 -m`
  
## 4.5. Set system clocks properly
**[DUT + Counterpart]**
```
  $ pmc -u -b 0 -t 1 "SET GRANDMASTER_SETTINGS_NP
  clockClass 248 clockAccuracy 0xfe offsetScaledLogVariance
  0xffff currentUtcOffset 37 leap61 0 leap59 0
  currentUtcOffsetValid 1 ptpTimescale 1
  timeTraceable 1 frequencyTraceable 0 timeSource 0xa0"
```

## 4.6. Disable NTP
**[DUT + Counterpart]**  
  `$ timedatectl set-ntp 0`

## 4.7. Synchronize System Clock with PTP hardware clock
**[DUT + Counterpart]**  
  `$ phc2sys -s enp1s0 -c CLOCK_REALTIME --step_threshold=1 --transportSpecific=1 -w -m`
  
## 4.8. Configure MQPRIO qdisc for 4 Tx queue with handle ID 8001
**[DUT]**
```
  $ tc qdisc add dev enp1s0 handle 8001: parent root mqprio \
  num_tc 4 map 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 3 \
  queues 1@0 1@1 1@2 1@3 hw 0
```

## 4.9. Install the ETF qdisc on Q3 and disable offload, if the NIC does not support it. 
Only CLOCK_TAI is supported. The delta parameter specifies how long before the transmission timestamp the ETF qdisc should send the frame to hardware. 
This test is rated as “proof of concept”, which leads to different results depending on the system that is tested. 
The parameters -d (=delta from wake up to txtime in nanoseconds) and -p (=run with RT priorty 'num') need to be set to a reasonable and system compliant value. 
Further investigations are ongoing for finding a reasonable value to get the best results.

**[DUT]**
```
  $ tc qdisc replace dev enp1s0 parent 8001:4 etf \
  clockid CLOCK_TAI delta 500000
```

## 4.10. Check if ETF is set up correctly
**[DUT]**
```
  $ tc qdisc show dev enp1s0

  qdisc mqprio 8001: root tc 4 map 0 1 2 3 3 3 3 3 3 3 3 3 3 3 3 3
  queues:(0:0) (1:1) (2:2) (3:3)
  qdisc fq_codel 0: parent 8001:3 limit 10240p flows 1024 quantum 1514 target 5.0ms interval 100.0ms memory_limit 32Mb ecn
  qdisc fq_codel 0: parent 8001:2 limit 10240p flows 1024 quantum 1514 target 5.0ms interval 100.0ms memory_limit 32Mb ecn
  qdisc fq_codel 0: parent 8001:1 limit 10240p flows 1024 quantum 1514 target 5.0ms interval 100.0ms memory_limit 32Mb ecn
  qdisc etf 8002: parent 8001:4 clockid TAI delta 500000 offload off deadline_mode off
```

## 4.11. Send packet in period of 1000000 ns, 500000 ns ahead of time
**[DUT]**  
  `$ udp_tai -i enp1s0 -P 1000000 -p 90 -d 500000`
  
## 4.12. Capture log
Start tcpdump and let it run for several minutes to capture the records. Then stop it by Ctr+C.  
**[Counterpart]**  
  `$ tcpdump -Q in -ttt -ni enp1s0 --time-stamp-precision=nano -j adapter_unsynced port 7788 > my.log`
  
## 4.13. Log file
The tcpdump logs inter-packet receive times in a log file. Each line starts with the packet time.
```
  $ tail my.log

   00:00:00.000999392 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000728 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000256 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999792 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000104 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999760 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999680 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000504 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000998848 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000288 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000616 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000000 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999728 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999752 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000320 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000512 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000024 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999200 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.001000368 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999976 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999888 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000999328 IP 169.254.247.160.7788 > 239.1.1.1.7788: UDP, length 256
```

## 4.14. Count log records
```
  $ wc -l my.log

  3790233 my.log
```

## 4.15. Check receive times
The inter-packet receive time should be close to 1000000 ns.

Example of result evaluation:
a. calculate total samples (S - Samples)
```
wc -l my.log
  S = 3219723 my.log  => 3.6M
```

b. calculate count of samples in the left interval <100-110) ms (IL - Inner Left interval)
```
egrep "^ 00:00:00\.0010" my.log | wc -l
  IL = 1679319  => 52%
```

c. calculate count of samples in the right interval <90-100) ms (IR - Inner Right interval)
```
egrep "^ 00:00:00\.0009" my.log | wc -l
  IR = 1540124  => 48%
```

d. calculate count of samples out of the interval <90-110) ms (O - Outer interval)
```
  O = S - IL - IR = 3219723 - 1679319 - 1540124 = 280 => less than 0,00009 %
```

e. check count of samples in the interval <110-200) ms (OR - Outer Right interval)
```
egrep "^ 00:00:00\.001[1-9]" my.log | wc -l
  102
=> OR = 102
```

f. check count of samples in the interval <200-1000) ms (ORR - Outer Right Right interval)
```
egrep "^ 00:00:00\.00[2-9]" my.log | wc -l
  1
=> ORR = 1
```

g. check count of samples in the interval <0-90) ms (OL - Outer Left interval)
```
egrep "^ 00:00:00\.000[0-8]" my.log | wc -l
  176
=> OL = 176
```

h. check count of all samples (expected 1)
```
  O - OR - ORR - OL = 280 - 102 - 1 - 176 = 1
```

i. What is the longest deviation (Dmax)
```
egrep -v "^ 00:00:00\.00[0-1][0-9]" my.log | sort -n | tail -n20
* 00:00:00.002001954 IP 169.254.112.164.7788 > 239.1.1.1.7788: UDP, length 256
=> Dmax = 100 - 200.2 = 100.2 ms
```

j. What is the shortest deviation (Dmin)
```
egrep "^ 00:00:00\.000[0-8]" my.log | sort -n | head -n20
   00:00:00.000000000 IP 169.254.112.164.7788 > 239.1.1.1.7788: UDP, length 256
-> 00:00:00.000525617 IP 169.254.112.164.7788 > 239.1.1.1.7788: UDP, length 256
   00:00:00.000550858 IP 169.254.112.164.7788 > 239.1.1.1.7788: UDP, length 256
=> Dmin = 100 - 52.5 = 47.5 ms
```

# References:
  [1]  Synchronizing Time with Linux* PTP, https://tsn.readthedocs.io/timesync.html
