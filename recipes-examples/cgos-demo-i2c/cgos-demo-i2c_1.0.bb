DESCRIPTION = "Congatec OS API (CGOS) examples - I2C example"
AUTHOR = "jan.rohacek@congatec.com"
SECTION = "console/utils"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"
PR = "r2"
DEPENDS = " cgos"
RDEPENDS_cgos-demo-i2c = " cgos"

SRC_URI = "file://CGOSDemo-I2C-1.0.tar.bz2 \
           file://COPYING \
          "

#S = "${WORKDIR}/${PN}-${PV}"
S = "${WORKDIR}/CGOSDemo-I2C-${PV}"

do_install() {
  install -d ${D}${bindir}
  install -m 777 ${S}/CGOSDemo-I2C ${D}${bindir}
}
